let showMore = document.getElementById("show-more-btn");


let title = document.getElementsByClassName("service-tabs-title");
let content = document.getElementsByClassName("service-tabs-content");
// for (let i=0; i<content[0].children.length; i++){
//     content[0].children[i].style.display = "none";
// }    
let currentActive = 0;
title[0].classList.add("active");
content[0].children[0].style.display = "flex";
for (let i=0; i<title.length; i++){
    title[i].addEventListener("click", ()=>{
        if(document.getElementsByClassName("active").length > 0){
            title[currentActive].classList.remove("active");
            content[0].children[currentActive].style.display = "none";
        }
        title[i].classList.add("active");
        content[0].children[i].style.display = "flex";
        currentActive = i;
});
}  

let div = document.getElementById('box');
let pictureBox = document.getElementById("pictureBox");

function createImg (classlist, folder, amount){
    for(let i=1; i<=amount; i++){
    // let imgWrap = document.createElement("div");
    let newImg = document.createElement("IMG");
    newImg.classList.add("all-img");
    newImg.classList.add(`${classlist}`);
    newImg.src=`img/${folder}/${classlist}${i}.jpg`;
    pictureBox.appendChild(newImg);
    // pictureBox.appendChild(imgWrap);
    // imgWrap.appendChild(newImg)
    }   
}

createImg("graphic-design", "graphicDesign", 12);
createImg("landing-page", "landingPage", 7);
createImg("web-design", "webDesign", 7);
createImg("wordpress", "wordpress", 10);


let workTitle = document.getElementsByClassName("work-tabs-title");
 workcurrentActive=0;
 workTitle[workcurrentActive].classList.add("active");

 for (let i=0; i<workTitle.length; i++){
    workTitle[i].addEventListener("click", ()=>{
        let allPict = document.querySelectorAll(".all-img");
        let imgGD = document.querySelectorAll(".graphic-design");
        let imgLP = document.querySelectorAll(".landing-page");
        let imgWD = document.querySelectorAll(".web-design");
        let imgWP = document.querySelectorAll(".wordpress");
        let arrImg =[allPict, imgGD, imgLP, imgWD, imgWP];
        showMore.hidden = true;
        if(document.getElementsByClassName("active").length > 0){
            workTitle[workcurrentActive].classList.remove("active");
            arrImg[workcurrentActive].forEach(function(element){
                element.style.display = "none";
            });
        if(i===0){
            workTitle[i].classList.add("active");
            showMore.hidden = false;
            arrImg[i].forEach(function(element){element.style.display = "block";});
            document.querySelectorAll(".all-img:nth-child(n+13)").forEach(function(element){element.style.display = 'none'});
            workcurrentActive = i;
        }else{
            workTitle[i].classList.add("active");
        arrImg[i].forEach(function(element){element.style.display = "block";});
        workcurrentActive = i ;
        }
;

};
});
}
console.dir(showMore);
showMore.addEventListener("click", ()=>{
    showMore.classList.add("loading");
    function showMoreFun(){
        showMore.classList.remove("loading");
    if(document.querySelectorAll(".all-img:nth-child(n+13):nth-child(-n+24)")[0].style.display === "block"){
        document.querySelectorAll(".all-img:nth-child(n+25)").forEach(function(element){element.style.display = 'block'});
        showMore.hidden = true;
    }
    document.querySelectorAll(".all-img:nth-child(n+13):nth-child(-n+24)").forEach(function(element){element.style.display = 'block'});
    }
    setTimeout(showMoreFun, 3000)
})



// let imgWrapper = document.getElementsByClassName("about-image-wrapper");
// let textWrapper = document.getElementsByClassName("about-text-wrapper");

// let btn = document.getElementsByClassName("about-btn");
// let bigImg = document.getElementsByClassName("about-bigImg");
// console.dir(btn);
// let count = 1;
// let prevImg = 0;


//     imgWrapper[0].children[prevImg].classList.add("about-image-active");
//     textWrapper[0].children[prevImg].style.display = "list-item";
// let interval = setInterval(()=>{
    
//     textWrapper[0].children[prevImg].style.display = "none";
//     imgWrapper[0].children[prevImg].classList.remove("about-image-active");
    

    
//     if (count >= imgWrapper[0].children.length){
//         count =0;
//     } 
//     bigImg[0].style.backgroundImage = `url(/img/about-${count+1}.png)`;
//     textWrapper[0].children[count].style.display = "list-item";
//     imgWrapper[0].children[count].classList.add("about-image-active");
//     prevImg = count;
//     count++;

// },1000);


$(document).ready(function(){
    $('.about-text-wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.about-image-content, .about-bigImg'

      });
    $('.about-bigImg').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.about-image-content, .about-text-wrapper'

      });
      $('.about-image-content').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.about-bigImg,.about-text-wrapper',
        autoplay: true,
        autoplaySpeed:2000,
        dots: false,
        centerMode: true,
        focusOnSelect: true
      });
      

                  
});




  